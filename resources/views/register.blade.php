<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <br>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
    @csrf
        <!-- Equivalent to... -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <label for="firstname">First Name :</label>
        <br>
        <input type="text" name='firstname' id="firstname">
        <br> <br>
    <label for="lastname">Last Name :</label>
        <br>
        <input type="text" name='lastname' id="lastname">
        <br><br>
    <label>Gender</label>
        <br>
        <input type="radio" id="pria" name="gender" value="Pria">
        <label for="pria">Pria</label>
        <br>
        <input type="radio" id="wanita" name="gender" value="Wanita">
        <label for="wanita">Wanita</label>
        <br><br>
    <label for="nationality">Nationality</label>
        <br>
        <select name="Nationality" id="nationality">
            <option value="1">None</option>
            <option value="2">Indonesia</option>
            <option value="3">Italia</option>
            <option value="4">Zimbabwe</option>
        </select>
        <br><br>
    <label for="">Language Spoken</label>
        <br>
        <input type="checkbox" id="indonesia" name="language">
        <label for="indonesia">Indonesia</label>
        <br>
        <input type="checkbox" id="english" name="language">
        <label for="english">English</label>
        <br>
        <input type="checkbox" id="other" name="language">
        <label for="other">Other</label>
        <br><br>
    <label for="bio">Bio</label>
        <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>    
        <br><br>
    <input type="submit" value="kirim">
    </form>
</body>
</html>